package spring.hometask.service;

import spring.hometask.domain.Auditorium;
import spring.hometask.repository.AuditoriumRepository;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Set;

public class AuditoriumServiceImpl implements AuditoriumService {
  private AuditoriumRepository auditoriumRepository;

  public AuditoriumServiceImpl(AuditoriumRepository auditoriumRepository) {
    this.auditoriumRepository = auditoriumRepository;
  }

  @Nonnull
  @Override
  public Set<Auditorium> getAll() {
    return (Set<Auditorium>) auditoriumRepository.getAll();
  }

  @Nullable
  @Override
  public Auditorium getByName(@Nonnull String name) {
    return auditoriumRepository.getByName(name);
  }
}
