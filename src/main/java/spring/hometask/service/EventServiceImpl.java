package spring.hometask.service;

import spring.hometask.domain.Event;
import spring.hometask.repository.EventRepository;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class EventServiceImpl implements EventService {
  private EventRepository eventRepository;

  public EventServiceImpl(EventRepository eventRepository) {
    this.eventRepository = eventRepository;
  }

  @Nullable
  @Override
  public Event getByName(@Nonnull String name) {
    return eventRepository.getByName(name);
  }

  @Override
  public Event save(@Nonnull Event object) {
    eventRepository.save(object);
    return object;
  }

  @Override
  public void remove(@Nonnull Event object) {
    eventRepository.remove(object);
  }

  @Override
  public Event getById(@Nonnull Long id) {
    return (Event) eventRepository.getById(id);
  }

  @Nonnull
  @Override
  public Collection<Event> getAll() {
    return eventRepository.getAll();
  }

  /**
   * getForDateRange(from, to) - returns events for specified date range (OPTIONAL) Returns events
   * airs for specified date range <code>fromDate</code> and <code>toDate</code> inclusive
   *
   * @param fromDate Start date to check
   * @param toDate End date to check
   * @return List events airs on date range
   */
  //  getForDateRange(from, to) - returns events for specified date range (OPTIONAL)
  public List<Event> getForDateRange(LocalDate fromDate, LocalDate toDate) {
    List<Event> events = (List<Event>) eventRepository.getAll();
    events.stream().filter(e -> e.airsOnDates(fromDate, toDate)).collect(Collectors.toList());
    return events;
  }

  /**
   * getNextEvents(to) - returns events from specified date (OPTIONAL) Returns events airs for
   * specified date range CurrentDate and <code>to</code> inclusive
   *
   * @param to End date to check
   * @return List events airs on date range
   */
  // getNextEvents(to) - returns events from now till the ‘to’ date (OPTIONAL)
  public List<Event> getNextEvents(LocalDate to) {
    LocalDate localDate = LocalDate.now();
    List<Event> events = (List<Event>) eventRepository.getAll();
    events.stream().filter(e -> e.airsOnDates(localDate, to)).collect(Collectors.toList());
    return events;
  }
}
