package spring.hometask.service;

import spring.hometask.domain.Event;
import spring.hometask.domain.User;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;

public class DiscountServiceImpl implements DiscountService {
  @Override
  public byte getDiscount(
      @Nullable User user,
      @Nonnull Event event,
      @Nonnull LocalDateTime airDateTime,
      long numberOfTickets) {
    return 10;
  }
}
