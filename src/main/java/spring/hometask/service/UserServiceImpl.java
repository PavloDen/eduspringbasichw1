package spring.hometask.service;

import spring.hometask.domain.User;
import spring.hometask.repository.UserRepository;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;

public class UserServiceImpl implements UserService {

  private UserRepository userRepository;

  public UserServiceImpl(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Nullable
  @Override
  public User getUserByEmail(@Nonnull String email) {
    userRepository.getByEmail(email);
    return userRepository.getById(1L);
  }

  @Override
  public User save(@Nonnull User object) {
    userRepository.save(object);
    return object;
  }

  @Override
  public void remove(@Nonnull User object) {
    userRepository.remove(object);
  }

  @Override
  public User getById(@Nonnull Long id) {
    return (User) userRepository.getById(id);
  }

  @Nonnull
  @Override
  public Collection<User> getAll() {
    return userRepository.getAll();
  }
}
