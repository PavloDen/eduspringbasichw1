package spring.hometask.service;

import spring.hometask.domain.Event;
import spring.hometask.domain.Ticket;
import spring.hometask.domain.User;
import spring.hometask.repository.AuditoriumRepository;
import spring.hometask.repository.EventRepository;
import spring.hometask.repository.TicketRepository;
import spring.hometask.repository.UserRepository;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class BookingServiceImpl implements BookingService {
  private TicketRepository ticketRepository;
  private UserRepository userRepository;
  private EventRepository eventRepository;
  private AuditoriumRepository auditoriumRepository;

  public BookingServiceImpl(
      TicketRepository ticketRepository,
      UserRepository userRepository,
      EventRepository eventRepository,
      AuditoriumRepository auditoriumRepository) {
    this.ticketRepository = ticketRepository;
    this.userRepository = userRepository;
    this.eventRepository = eventRepository;
    this.auditoriumRepository = auditoriumRepository;
  }

  @Override
  public double getTicketsPrice(
      @Nonnull Event event,
      @Nonnull LocalDateTime dateTime,
      @Nullable User user,
      @Nonnull Set<Long> seats) {
    if (event.getAirDates().equals(dateTime)) {
      return event.getBasePrice() * seats.size();
    }
    return 0;
  }

  @Override
  public void bookTickets(@Nonnull Set<Ticket> tickets) {
    User user = new User();
    if (!tickets.isEmpty()) {
      for (Ticket ticket : tickets) {
        user = ticket.getUser();
        if (user != null) {
          userRepository.save(user);
          ticketRepository.save(ticket);
        }
      }
    }
  }

  @Nonnull
  @Override
  public Set<Ticket> getPurchasedTicketsForEvent(
      @Nonnull Event event, @Nonnull LocalDateTime dateTime) {
    if (event != null) {
      List<Ticket> allTickets = (List<Ticket>) ticketRepository.getAll();
      Set<Ticket> ticketsPurchased =
          allTickets.stream()
              .filter(ticket -> ticket.getUser() != null)
              .filter(ticket -> event.equals(ticket.getEvent()))
              .filter(ticket -> ticket.getEvent().airsOnDateTime(dateTime))
              .collect(Collectors.toSet());
      //    for (Ticket t : allTickets) {
      //      if (t.getUser() != null)
      //        if (t.getEvent().airsOnDateTime(dateTime)) {
      //          if (t.getEvent().equals(event)) {
      //            ticketsPurchased.add(t);
      //          }
      //        }
      //    }
      return ticketsPurchased;
    }
    return null;
  }
}
