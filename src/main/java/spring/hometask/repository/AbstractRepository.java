package spring.hometask.repository;

import javax.annotation.Nonnull;
import java.util.Collection;

public interface AbstractRepository<T> {

  T save(@Nonnull T object);

  void remove(@Nonnull T object);

  T getById(@Nonnull Long id);

  @Nonnull
  Collection<T> getAll();
}
