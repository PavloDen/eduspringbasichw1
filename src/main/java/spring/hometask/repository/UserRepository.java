package spring.hometask.repository;

import spring.hometask.domain.*;

import javax.annotation.Nonnull;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static java.util.Collections.EMPTY_SET;

public class UserRepository implements AbstractRepository<User> {
  private List<User> users = new ArrayList<>();

  public void init() {

    NavigableSet<LocalDateTime> localDateTimes = new TreeSet<>();
    String str = "2019-11-11 12:00";
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    LocalDateTime dateTime = LocalDateTime.parse(str, formatter);

    localDateTimes.add(dateTime);
    NavigableMap<LocalDateTime, Auditorium> auditoriums = new TreeMap<>();
    Auditorium auditorium = new Auditorium();
    auditorium.setNumberOfSeats(111L);
    auditorium.setVipSeats(EMPTY_SET);
    auditorium.setName("Test1");
    auditoriums.put(localDateTimes.first(), auditorium);

    Event event = new Event("Event1", localDateTimes, 11, EventRating.HIGH, auditoriums);

    User user = new User("Fname", "Lname", "mail@mail.com", null);

    Ticket ticket = new Ticket(user, event, localDateTimes.first(), 10);
    NavigableSet<Ticket> tickets = new TreeSet<>();
    tickets.add(ticket);

    user.setTickets(tickets);
    user.setId(1L);
    users.add(user);
    users.add(new User("F2", "L2", "2@mail.com", null));
    users.add(new User("F33", "L33", "3@mail.com", null));
  }

  public User getByEmail(String email) {
    return users.stream().filter(user -> user.getEmail().equals(email)).findFirst().orElse(null);
  }

  @Override
  public User save(@Nonnull User object) {
    users.add(object);
    return object;
  }

  @Override
  public void remove(@Nonnull User object) {
    users.remove(object);
  }

  @Override
  public User getById(@Nonnull Long id) {
    return users.stream().filter(user -> user.getId().equals(id)).findFirst().orElse(null);
  }

  @Nonnull
  @Override
  public Collection<User> getAll() {
    return users;
  }
}
