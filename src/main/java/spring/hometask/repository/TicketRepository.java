package spring.hometask.repository;

import spring.hometask.domain.*;

import javax.annotation.Nonnull;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class TicketRepository implements AbstractRepository<Ticket> {

  private List<Ticket> tickets = new ArrayList<>();

  public void init() {
    User user = new User("Fname", "Lname", "mail@mail.com", null);
    user.setId(1L);

    NavigableSet<LocalDateTime> localDateTimes = new TreeSet<>();

    String str = "2019-11-11 12:00";
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    LocalDateTime dateTime = LocalDateTime.parse(str, formatter);

    localDateTimes.add(dateTime);
    NavigableMap<LocalDateTime, Auditorium> auditoriums = new TreeMap<>();
    auditoriums.put(localDateTimes.first(), new Auditorium());
    Event event = new Event("Event1", localDateTimes, 11, EventRating.HIGH, auditoriums);
    Ticket ticket = new Ticket(user, event, localDateTimes.first(), 10);
    this.save(ticket);
  }

  @Override
  public Ticket save(@Nonnull Ticket object) {
    tickets.add(object);
    return object;
  }

  @Override
  public void remove(@Nonnull Ticket object) {
    tickets.remove(object);
  }

  @Override
  public Ticket getById(@Nonnull Long id) {
    return tickets.stream().filter(ticket -> ticket.getId().equals(id)).findFirst().orElse(null);
  }

  @Nonnull
  @Override
  public Collection<Ticket> getAll() {
    return tickets;
  }
}
