package spring.hometask.repository;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import spring.hometask.domain.Auditorium;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.IOException;
import java.util.*;

import static java.util.Collections.EMPTY_SET;

public class AuditoriumRepository {
  private Set<Auditorium> auditoriums = new HashSet<>();
  private String fileName;
  private File file;
  private static ObjectMapper objectMapper = new ObjectMapper();

  public AuditoriumRepository(String fileName) {
    this.fileName = fileName;
    this.file = new File(fileName);
  }

  public AuditoriumRepository() {}

  public void init() {

    if (!file.exists()) {
      throw new IllegalArgumentException("Can not open file with Auditoriums : " + fileName);
    }
    try {
      auditoriums.addAll(getAuditoriumsFromFile(file));
    } catch (IOException e) {
      e.printStackTrace();
    }

    Auditorium auditorium = new Auditorium();
    auditorium.setNumberOfSeats(111L);
    auditorium.setVipSeats(EMPTY_SET);
    auditorium.setName("Test1");
    save(auditorium);
  }

  public static Collection<Auditorium> getAuditoriumsFromFile(File jsonFile) throws IOException {
    Auditorium[] response = objectMapper.readValue(jsonFile, Auditorium[].class);
    return new HashSet<>(Arrays.asList(response));
  }

  public Auditorium save(@Nonnull Auditorium object) {
    auditoriums.add(object);
    if (!file.canWrite()) {
      System.out.println("Can not write to file: " + fileName);
    } else {
      try (JsonGenerator jGenerator =
          objectMapper.getFactory().createGenerator(new File(fileName), JsonEncoding.UTF8)) {
        jGenerator.useDefaultPrettyPrinter();
        jGenerator.writeObject(auditoriums);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return object;
  }

  public void remove(@Nonnull Auditorium object) {
    auditoriums.remove(object);
    Collection<Auditorium> auditoriumsFromFile = new HashSet<>();
    try {
      auditoriumsFromFile.addAll(getAuditoriumsFromFile(file));
    } catch (IOException e) {
      e.printStackTrace();
    }
    auditoriumsFromFile.remove(object);
    auditoriumsFromFile.forEach(
        o -> {
          try {
            FileUtils.writeStringToFile(file, o.toString() + "\n", "UTF8", true);
          } catch (IOException e) {
            e.printStackTrace();
          }
        });
  }

  public Auditorium getByName(@Nonnull String name) {
    return auditoriums.stream().filter(a -> a.getName().equals(name)).findFirst().orElse(null);
  }

  @Nonnull
  public Collection<Auditorium> getAll() {
    return auditoriums;
  }
}
