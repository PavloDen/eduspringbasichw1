package spring.hometask.repository;

import spring.hometask.domain.Auditorium;
import spring.hometask.domain.Event;
import spring.hometask.domain.EventRating;

import javax.annotation.Nonnull;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class EventRepository implements AbstractRepository<Event> {

  private List<Event> events = new ArrayList<>();

  public void init() {

    NavigableSet<LocalDateTime> localDateTimes = new TreeSet<>();
    String strDate = "2019-11-11 12:00";
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    LocalDateTime dateTime = LocalDateTime.parse(strDate, formatter);

    localDateTimes.add(dateTime);
    NavigableMap<LocalDateTime, Auditorium> auditoriums = new TreeMap<>();
    auditoriums.put(localDateTimes.first(), new Auditorium());
    Event event = new Event("Event1", localDateTimes, 11, EventRating.HIGH, auditoriums);
    events.add(event);

    events.add(new Event("Ev2", localDateTimes, 22, EventRating.MID, auditoriums));
    events.add(new Event("Ev3", localDateTimes, 33, EventRating.LOW, auditoriums));
  }

  public Event getByName(@Nonnull String name) {
    return events.stream().filter(event -> event.getName().equals(name)).findFirst().orElse(null);
  }

  @Override
  public Event save(@Nonnull Event object) {
    events.add(object);
    return object;
  }

  @Override
  public void remove(@Nonnull Event object) {
    events.remove(object);
  }

  @Override
  public Event getById(@Nonnull Long id) {
    return events.stream().filter(event -> event.getId().equals(id)).findFirst().orElse(null);
  }

  @Nonnull
  @Override
  public Collection<Event> getAll() {
    return events;
  }
}
