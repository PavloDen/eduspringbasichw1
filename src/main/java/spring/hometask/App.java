package spring.hometask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import spring.hometask.repository.TicketRepository;
import spring.hometask.service.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class App {
  private static final Logger LOGGER = LogManager.getLogger(App.class);

  public static void main(String[] args) {

    // using Spring for create user
    ConfigurableApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
    App app = (App) ctx.getBean("app");

    UserService userService = ctx.getBean(UserServiceImpl.class);
    LOGGER.info("All users");
    LOGGER.info(String.valueOf(userService.getAll()));

    EventService eventService = ctx.getBean(EventServiceImpl.class);
    LOGGER.info("***********");
    LOGGER.info("All events");
    LOGGER.info(String.valueOf(eventService.getAll()));

    TicketRepository ticketRepository = ctx.getBean(TicketRepository.class);
    LOGGER.info("***********");
    LOGGER.info("All tickets");
    LOGGER.info(String.valueOf(ticketRepository.getAll()));

    BookingService bookingService = ctx.getBean(BookingServiceImpl.class);
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
    String str = "2019-11-11 12:00";
    LocalDateTime dateTime = LocalDateTime.parse(str, formatter);
    LOGGER.info("***********");
    LOGGER.info("All tickets sold for Event1");
    LOGGER.info(
        String.valueOf(
            bookingService.getPurchasedTicketsForEvent(
                eventService.getByName("Event1"), dateTime)));

    AuditoriumService auditoriumService = ctx.getBean(AuditoriumServiceImpl.class);
    LOGGER.info("***********");
    LOGGER.info("All Auditoriums");
    LOGGER.info(String.valueOf(auditoriumService.getAll()));
  }
}
